# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.reject { |char| char == char.downcase }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle_index = str.size / 2
  str[(middle_index - (str.size.even? ? 1 : 0))..middle_index]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count(VOWELS.to_s)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  num == 1 ? 1 : num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr.reduce { |collector, item| collector + separator + item } || ''
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  (0...str.size).reduce('') do |accumulator, idx|
    accumulator + str[idx].method(idx.odd? ? :upcase : :downcase).call
  end
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.reduce([]) do |accumulator, word|
    accumulator << (word.size >= 5 ? word.reverse : word)
  end.join ' '
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  1.upto(n).reduce([]) do |accumulator, num|
    accumulator << if (num % 15).zero?
      'fizzbuzz'
    elsif (num % 5).zero?
      'buzz'
    elsif (num % 3).zero?
      'fizz'
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  (0...arr.size).each_with_object([]) do |index, memo|
    memo[arr.size - 1 - index] = arr[index]
  end
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  2.upto((num**0.5).to_i).none? { |n| (num % n).zero? }
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |n| (num % n).zero? }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select { |n| (num % n).zero? && prime?(n) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  arr.select { |num| num.method(arr.count(&:even?) > 1 ? :odd? : :even?).call }.first
end
